# -*- coding: utf-8 -*-
import vk
import urllib
import json
import time

class Worker:
    users_list=[]
    already_send=[]

    receivers=[]

    def send_messages(self):
        session = vk.AuthSession('app_id', 'login', 'pass', scope='messages')
        vk_api = vk.API(session)

        for user in self.receivers:
            try:
                vk_api.messages.send(user_id=user, message='Clickjacking - механизм обмана пользователей интернета, при котором злоумышленник может получить доступ к конфиденциальной информации или даже получить доступ к компьютеру пользователя, заманив его на внешне безобидную страницу или внедрив вредоносный код на безопасную страницу', v=5.73)
                print("Отправлено сообщение пользователю %s"%(user))
            except:
                print("Не удалось отправить сообщение пользователю %s"%(user))
            self.already_send.append(user)

    def get_receivers(self):
        self.receivers=[]
        for user in self.users_list:
            if user not in self.already_send:
                self.receivers.append(user)

        if len(self.receivers)!=0:
            self.send_messages()

    def get_user_list(self):
        data=urllib.urlopen("https://api.vk.com/method/likes.getList?type=sitepage&owner_id=app_id&page_url=app_uri&offset=0&count=0&version=5.74").read()
        data_json=json.loads(data)
        users=data_json["response"]["users"]
        self.users_list=users
        #print(self.users_list)

if __name__ == '__main__':
    bot=Worker()
    while True:
        bot.get_user_list()
        bot.get_receivers()
        time.sleep(15)
